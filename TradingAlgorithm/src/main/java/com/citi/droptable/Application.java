package com.citi.droptable;


import com.citi.droptable.rest.classes.BrokerEntity;
import com.citi.droptable.rest.jms.Producer;
import com.citi.droptable.rest.handler.StrategyHandler;
import io.swagger.models.auth.In;
import org.apache.activemq.broker.Broker;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.FileSystemUtils;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;

@Configuration
@EnableScheduling
@ComponentScan
@EnableJms
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class Application {

    @Bean
        // Strictly speaking this bean is not necessary as boot creates a default
    JmsListenerContainerFactory<?> myJmsContainerFactory(ConnectionFactory connectionFactory) {
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        return factory;
    }

    public static void main(String[] args) {



        // Clean out any ActiveMQ data from a previous run
        FileSystemUtils.deleteRecursively(new File("activemq-data"));

        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        StrategyHandler handler = context.getBean(StrategyHandler.class);

        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        Producer p = new Producer(handler);
        try {
            handler.checkDatabase();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        handler.getScheduler().getPool().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Map<String, Integer> map = handler.getStockMap();
                List<BrokerEntity> brokerEntityList=  p.getForQueueForBroker(map);
                p.sendMessage(jmsTemplate, brokerEntityList);
            }
        },5000,2000, TimeUnit.MILLISECONDS);



    }
}
