package com.citi.droptable.TradingAlgo;

import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;

import java.sql.Date;
import java.sql.Time;

/**
 * Price Breakout is one of the traiding strategies.
 */
public class PriceBreakout extends TradingStrat{
    private double open;
    private double close;
    private double high;
    private double low;
    private int numberOfOccurences;

    public PriceBreakout() {
    }

    public PriceBreakout(String name, int type, double shortAvgTime, double longAvgTime, double stdDeviationTime, int numberofOccur, Time time, Date date, String stock, int volume, double amount, double profit, double loss, boolean active) {
        super(name, type, shortAvgTime, longAvgTime, stdDeviationTime, numberofOccur, time, date, stock, volume, amount, profit, loss, active);
    }
}
