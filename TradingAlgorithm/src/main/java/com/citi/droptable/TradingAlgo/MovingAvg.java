package com.citi.droptable.TradingAlgo;

import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * This class is one of the trading strategies.
 * Vincent Taylor
 *
 */

public class MovingAvg extends TradingStrat{
    private BigDecimal shortAvgOld;
    private BigDecimal longAvg = new BigDecimal(0);
    private BigDecimal shortAvgNew= new BigDecimal(0);
    private int numberOfShortsPerLong=0;
    private int counter=0;
    private Logger logger = Logger.getLogger(this.getClass());
    static MathContext mc = new MathContext(9, RoundingMode.HALF_DOWN);

    public MovingAvg(String name, int type, double shortAvgTime, double longAvgTime, double stdDeviationTime, int numberofOccur, Time time, Date date, String stock, int volume, double amount, double profit, double loss, boolean active) {
        super(name, type, shortAvgTime, longAvgTime, stdDeviationTime, numberofOccur, time, date, stock, volume, amount, profit, loss, active);
        this.numberOfShortsPerLong = (int)Math.ceil(longAvgTime/shortAvgTime);
    }

    public BigDecimal getShortAvgOld() {
        return shortAvgOld;
    }

    public void setShortAvgOld(BigDecimal shortAvgOld) {
        this.shortAvgOld = shortAvgOld;
    }

    public BigDecimal getLongAvg() {
        return longAvg;
    }

    public void setLongAvg(BigDecimal longAvg) {
        this.longAvg = longAvg;
    }

    public BigDecimal getShortAvgNew() {
        return shortAvgNew;
    }

    public void setShortAvgNew(BigDecimal shortAvgNew) {
        this.shortAvgNew = shortAvgNew;
    }

    public static MathContext getMc() {
        return mc;
    }

    public static void setMc(MathContext mc) {
        MovingAvg.mc = mc;
    }


    @Override
    public void update(List<Double> prices) {
        BigDecimal sum = new BigDecimal(0);
        for (int i = 0; i<getSeconds(getShortAvgTime());i++){
            sum = sum.add( BigDecimal.valueOf(prices.get(i)), mc); //Computes Small Average
        }
            shortAvgOld = shortAvgNew;
            shortAvgNew = sum.divide(BigDecimal.valueOf(getSeconds(getShortAvgTime())),mc);
            counter++;
            counter%=numberOfShortsPerLong;
        sum = new BigDecimal(0);
        if(counter == 0) {
            for (int i = 0; i < getSeconds(getLongAvgTime()); i++) {
                sum = sum.add(BigDecimal.valueOf(prices.get(i)), mc);
            }
            longAvg = sum.divide(BigDecimal.valueOf(getSeconds(getLongAvgTime())), mc); //Computes Long Average
        }
    }



    @Override
    public int execute(double price, String stock, int volume) {
        logger.debug("Executing: "+getName() );
        if(shortAvgOld == null){
            update(getPrices(getStock(),(int)getSeconds(getLongAvgTime())));
        }
//        if(!isActive())
//            return 0;
        if(!(stock.equalsIgnoreCase(getStock()))) {
            return 0;
        }


        if(shortAvgOld.compareTo(longAvg)>0 && shortAvgNew.compareTo(longAvg)==1){ //Checks conditions for buy
            if(getVolume()<=0 || getVolume()-volume < 0) //Makes sure we have the volume to sell
                return 0;
            setAmount(getAmount()+(price*volume));
            setVolume(getVolume()-volume);
            return 1;
        }
        if(shortAvgOld.compareTo(longAvg) ==-1 && shortAvgNew.compareTo(longAvg)>0){//Checks conditions for sell
            if(getAmount()<=0 || getAmount()-(price*volume) < 0) //Make sure we have the money to buy
                return 0;
            setAmount(getAmount()-(price*volume));
            setVolume(getVolume()+volume);
            return -1;
        }
        return 0;
    }


    @Override
    public void run() {

        logger.debug(getName() + " has started to update at time: "+new Time(System.currentTimeMillis()).toString());
        update(getPrices(getStock(),(int)getSeconds(getLongAvgTime())));
    }
}
