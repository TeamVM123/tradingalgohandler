package com.citi.droptable.TradingAlgo;

import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * This class is one of the trading strategies.
 */
public class Bollinger extends TradingStrat {
    private Logger logger = Logger.getLogger(this.getClass());
    //sma
    private BigDecimal sma;
    //upper standard deviation
    private BigDecimal upperBand;
    //lower standard deviation
    private BigDecimal lowerBand;
    private BigDecimal strDeviation;
    static MathContext mc = new MathContext(9, RoundingMode.HALF_DOWN);

    public Bollinger(String name, int type, double shortAvgTime, double longAvgTime, double stdDeviationTime, int numberofOccur, Time time, Date date, String stock, int volume, double amount, double profit, double loss, boolean active) {
        super(name, type, shortAvgTime, longAvgTime, stdDeviationTime, numberofOccur, time, date, stock, volume, amount, profit, loss, active);
    }

    @Override
    public int execute(double price,String Stock,int volume) {
        //update(getPrices(getStock(),(int)getSeconds(getStdDeviationTime())));
        if(this.upperBand == null || this.lowerBand == null || this.strDeviation==null){
            update(getPrices(getStock(),(int)getSeconds(getStdDeviationTime())));
        }
        logger.debug("*******Bollinger info before execute:");
        logger.debug("upper band : "+ this.upperBand);
        logger.debug("lower band : " + this.lowerBand);
        logger.debug("New STDCALCULATION "+this.strDeviation);
        BigDecimal priceBig = new BigDecimal(price);
        logger.debug("current price of stock: "+ priceBig);
        if(!isActive()) {
            logger.info("Bollinger execute status: Not acive");
            return 0;
        }
        if(priceBig.compareTo(this.upperBand)<0 && priceBig.compareTo(this.lowerBand) >0 ){
            logger.info("Bollinger execute status: --BETWEEN BANDS");
            return 0;
        } else if(priceBig.compareTo(this.lowerBand)< 0){
            //price < lower than buy
            if(getAmount()<=0 || getAmount()-(price*volume) < 0)
                return 0;
            setAmount(getAmount()-(price*volume));
            setVolume(getVolume()+volume);
            logger.info("Bollinger execute status: PRICE LOWER THAN LOW BAND --- BUY");
            return 1;
        }else if (priceBig.compareTo(this.upperBand)> 0){
            //price> upper so sell
            if(getVolume()<=0 || getVolume()-volume < 0)
                return 0;
            setAmount(getAmount()+(price*volume));
            setVolume(getVolume()-volume);
            logger.info("Bollinger execute status: PRICE HIGHER THAN HIGH BAND --SELL");
            return -1;
        }else{
            return 0;
        }
    }


    public BigDecimal getSma() {
        return sma;
    }

    public void setSma(BigDecimal sma) {
        this.sma = sma;
    }

    public BigDecimal getUpperBand() {
        return upperBand;
    }

    public void setUpperBand(BigDecimal upperBand) {
        this.upperBand = upperBand;
    }

    public BigDecimal getLowerBand() {
        return lowerBand;
    }

    public void setLowerBand(BigDecimal lowerBand) {
        this.lowerBand = lowerBand;
    }

    public BigDecimal getStrDeviation() {
        return strDeviation;
    }

    public void setStrDeviation(BigDecimal strDeviation) {
        this.strDeviation = strDeviation;
    }

    public static MathContext getMc() {
        return mc;
    }

    public static void setMc(MathContext mc) {
        Bollinger.mc = mc;
    }



    @Override
    public void update(List<Double> prices) {
        this.sma = getSma(prices);
        this.strDeviation = getStdDeviationCalculation(prices,sma);
        BigDecimal two = new BigDecimal(0.05);
        BigDecimal stdCommon = two.multiply(this.strDeviation);
        this.upperBand = this.sma.add(stdCommon);
        this.lowerBand = this.sma.subtract(stdCommon);
    }

    public BigDecimal getStdDeviationCalculation(List<Double> prices, BigDecimal sma) {

        BigDecimal error = new BigDecimal(0.0);
        if (prices == null || prices.size() == 0) {
           return error;
        }
        if (sma.compareTo(new BigDecimal(0))==0) { //error check because it should never be 0
            return error;
        }

        BigDecimal std = new BigDecimal(1);
        BigDecimal stdTime = new BigDecimal(this.getSeconds(this.getStdDeviationTime()));
        BigDecimal one = new BigDecimal(1);

        for (int i = 0; i < prices.size(); i++) {
            BigDecimal price = new BigDecimal(prices.get(i));
            //BigDecimal smaBigDecimal = new BigDecimal(sma.doubleValue(), mc);
            std = std.add(sma.subtract(price, mc).pow(2));
        }
        BigDecimal newTime = stdTime.subtract(one);
        BigDecimal result1 = std.divide(newTime,mc);
        BigDecimal result = new BigDecimal(Math.sqrt(result1.doubleValue()), mc);


        return result;
    }

    public BigDecimal getSma(List<Double> prices) {
        BigDecimal listLength = new BigDecimal(prices.size());
        double sum = 0;
        for (int i = 0; i < prices.size(); i++) {
            sum += prices.get(i);
        }
        BigDecimal div = new BigDecimal(sum);
        BigDecimal result = div.divide(listLength,mc);
        return result;
    }


    @Override
    public void run()
    {
        logger.debug(getName() + " has started to update at time: "+new Time(System.currentTimeMillis()).toString());
        update(getPrices(getStock(),(int)getSeconds(getStdDeviationTime())));
    }
}
