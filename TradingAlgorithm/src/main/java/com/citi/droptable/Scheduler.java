package com.citi.droptable;

import java.util.concurrent.*;
/**
 * This is the confiuration of the Thread Scehduling pool used to run the update mathods for each of the strategies
 * Vincent Taylor
 *
 */


public class Scheduler implements ThreadFactory {

    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(30); //Thread pool size
    int threadPoolSize = 30;

    public  ScheduledThreadPoolExecutor getPool() {
        return scheduledThreadPoolExecutor;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r);
    }


    public Scheduler(){

    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public void setScheduledThreadPoolExecutor(ScheduledThreadPoolExecutor scheduledThreadPoolExecutor) {
        this.scheduledThreadPoolExecutor = scheduledThreadPoolExecutor;
    }
}