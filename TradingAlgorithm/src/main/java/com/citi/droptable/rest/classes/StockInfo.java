package com.citi.droptable.rest.classes;

import java.sql.Time;

/**
 * This object gives us all information that a stock has from the market
 * Vincent Taylor
 */

public class StockInfo {
    private String symbol;
    private int symbolID;
    private double price;
    private Time theTime;
    private int periodNumber;
    private String companyName;


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getSymbolID() {
        return symbolID;
    }

    public void setSymbolID(int symbolID) {
        this.symbolID = symbolID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Time getTheTime() {
        return theTime;
    }

    public void setTheTime(Time theTime) {
        this.theTime = theTime;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public StockInfo(String symbol, int symbolID, double price, Time theTime, int periodNumber, String companyName) {
        this.symbol = symbol;
        this.symbolID = symbolID;
        this.price = price;
        this.theTime = theTime;
        this.periodNumber = periodNumber;
        this.companyName = companyName;
    }
    public StockInfo(){

    }
}
