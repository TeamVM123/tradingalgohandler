package com.citi.droptable.rest.classes;


import com.citi.droptable.rest.handler.Consumer;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * This class is a more is the general super class to all of the trading strategy
 *
 * Vincent Taylor
 */

public class TradingStrat extends Consumer implements Runnable{
    private String name;
    private int strategyId;
    private double shortAvgTime;
    private double longAvgTime;
    private double stdDeviationTime;
    private int numberofOccur;
    private Time time;
    private Date date;
    private String Stock;
    private int volume;
    private double amount;
    private double profit;
    private boolean active;
    private double loss;

    public int getNumberofOccur() {
        return numberofOccur;
    }

    public void setNumberofOccur(int numberofOccur) {
        this.numberofOccur = numberofOccur;
    }

    public String getStock() {
        return Stock;
    }

    public void setStock(String stock) {
        Stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getStrategyid() {
        return strategyId;
    }

    public void setStrategyId(int type) {
        this.strategyId = type;
    }

    public double getShortAvgTime() {
        return shortAvgTime;
    }

    public void setShortAvgTime(double shortAvgTime) {
        this.shortAvgTime = shortAvgTime;
    }

    public double getLongAvgTime() {
        return longAvgTime;
    }

    public void setLongAvgTime(double longAvgTime) {
        this.longAvgTime = longAvgTime;
    }

    public double getStdDeviationTime() {
        return stdDeviationTime;
    }

    public void setStdDeviationTime(double stdDeviationTime) {
        this.stdDeviationTime = stdDeviationTime;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getLoss() {
        return loss;
    }

    public void setLoss(double loss) {
        this.loss = loss;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getSeconds(double time){
        return Math.ceil(time/1000);
    }

    public TradingStrat() {
    }

    public TradingStrat(String name, int type, double shortAvgTime, double longAvgTime, double stdDeviationTime, int numberofOccur, Time time, Date date, String stock, int volume, double amount, double profit, double loss, boolean active) {
        this.name = name;
        this.strategyId = type;
        this.shortAvgTime = shortAvgTime;
        this.longAvgTime = longAvgTime;
        this.stdDeviationTime = stdDeviationTime;
        this.numberofOccur = numberofOccur;
        this.time = time;
        this.date = date;
        Stock = stock;
        this.volume = volume;
        this.amount = amount;
        this.profit = profit;
        this.loss = loss;
        this.active = active;
    }

    public int execute(double price, String Stock,int volume){return 0;}
    public void update(List<Double> prices){return;}

    @Override
    public void run() {

    }
}
