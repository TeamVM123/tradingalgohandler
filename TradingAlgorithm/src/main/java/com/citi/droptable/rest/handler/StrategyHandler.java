package com.citi.droptable.rest.handler;

import com.citi.droptable.TradingAlgo.Bollinger;
import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.TradingAlgo.PriceBreakout;
import com.citi.droptable.Scheduler;
import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Schedules threads to run (Stock updates, database polls, and status polls)
 * Vincent Taylor
 *
 */


@Component
public class StrategyHandler{
    private Map<String,Bollinger> bollingers;
    private Map<String,MovingAvg> movingAvg;
    private Map<String,PriceBreakout> priceBreakout;
    private ArrayList<String> addedId;
    private Scheduler scheduler;
    private Map<String,Integer> stockMap;
    final static Logger logger = Logger.getLogger(StrategyHandler.class);

    @Value("${db.url}") //Auto injects path to rest api
    private String path;

    public Map<String, Bollinger> getBollingers() {
        return bollingers;
    }

    public Map<String, MovingAvg> getMovingAvg() {
        return movingAvg;
    }

    public Map<String, PriceBreakout> getPriceBreakout() {
        return priceBreakout;
    }


    public ArrayList<String> getAddedId() {
        return addedId;
    }

    public Map<String, Integer> getStockMap() {
        return stockMap;
    }



    public StrategyHandler() {
        this.scheduler= new Scheduler();
        this.addedId = new ArrayList<>();
        this.stockMap = new LinkedHashMap<>();
        this.bollingers = new HashMap<>();
        this.movingAvg = new HashMap<>();
        this.priceBreakout = new HashMap<>();

        scheduler.getPool().scheduleAtFixedRate(new Runnable() { //Schedules the polling of the database for new strategies
            @Override
            public void run() {
                try {
                    checkDatabase();
                } catch (JSONException e) {
                    logger.fatal(e);
                    e.printStackTrace();
                } catch (IOException e) {
                    logger.fatal(e);
                    e.printStackTrace();
                }
            }
        },10000,10000,TimeUnit.MILLISECONDS);
        scheduler.getPool().scheduleAtFixedRate(new Runnable() {//Schedules the polling of the database for changes in status
            @Override
            public void run() {
                try {
                    checkDatabaseActive();
                } catch (JSONException e) {
                    e.printStackTrace();
                    logger.fatal(e);
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.fatal(e);
                }
            }
        },10000,10000,TimeUnit.MILLISECONDS);
    }



    public String getAllRequest(){ //Gets all of the strategies in the database
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(path+":8080/api/StrategyInfo/" );
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                logger.error("Failed : HTTP error code : "
                        + conn.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());

            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();
            logger.error(e);

        } catch (IOException e) {

            e.printStackTrace();
            logger.error(e);

        }
        return response.toString();

    }


    public void checkDatabaseActive() throws JSONException, IOException { //Checks the strategy table to see if any status have changed
        logger.debug("Checking to see if any strategies have gone offline");
        JSONArray jsonArr = null;
        try {
            jsonArr = new JSONArray(getAllRequest());
        } catch (JSONException e) {
            e.printStackTrace();
            logger.error(e);
        }
        TradingStrat commonStrat;
        for (int i = 0; i < jsonArr.length(); i++) {
            commonStrat = (new ObjectMapper().readValue(jsonArr.getJSONObject(i).toString(), TradingStrat.class));
            String id = commonStrat.getName();
            Bollinger b = bollingers.getOrDefault(id,null);
            MovingAvg m = movingAvg.getOrDefault(id,null);
            PriceBreakout p = priceBreakout.getOrDefault(id,null);
            if(!(b==null)&& b.isActive()!=commonStrat.isActive())
                b.setActive(!b.isActive());
            if(!(m==null)&& m.isActive()!=commonStrat.isActive())
                m.setActive(!m.isActive());
            if(!(p==null)&& p.isActive()!=commonStrat.isActive())
                p.setActive(!p.isActive());


        }
        //  System.out.println(bollingers.get("vince1").isActive());

    }

    public void checkDatabase() throws JSONException, IOException { //Checks the strategy table for new strategies
        logger.debug("Checking the Database for new entries");
        JSONArray jsonArr = null;
        try {
            jsonArr = new JSONArray(getAllRequest());//getAllRequest());
        } catch (JSONException e) {
            e.printStackTrace();
            logger.error(e);
        }
        TradingStrat commonStrat;
        for (int i = 0; i < jsonArr.length(); i++) {
            commonStrat = (new ObjectMapper().readValue(jsonArr.getJSONObject(i).toString(), TradingStrat.class));
            if(commonStrat.getStrategyid()==0) {
                if(!bollingers.containsKey(commonStrat.getName())) {
                    bollingers.put(commonStrat.getName(),new Bollinger( commonStrat.getName(), commonStrat.getStrategyid(), commonStrat.getShortAvgTime(), commonStrat.getLongAvgTime(), commonStrat.getStdDeviationTime(), commonStrat.getNumberofOccur(), commonStrat.getTime(), commonStrat.getDate(), commonStrat.getStock(), commonStrat.getVolume(), commonStrat.getAmount(), commonStrat.getProfit(), commonStrat.getLoss(), commonStrat.isActive()));
                    logger.info("Found new strategy: "+ commonStrat.getName());
                }
            }
            if(commonStrat.getStrategyid()==1) {
                if(!movingAvg.containsKey(commonStrat.getName())) {
                    movingAvg.put(commonStrat.getName(),new MovingAvg( commonStrat.getName(), commonStrat.getStrategyid(), commonStrat.getShortAvgTime(), commonStrat.getLongAvgTime(), commonStrat.getStdDeviationTime(), commonStrat.getNumberofOccur(), commonStrat.getTime(), commonStrat.getDate(), commonStrat.getStock(), commonStrat.getVolume(), commonStrat.getAmount(), commonStrat.getProfit(), commonStrat.getLoss(), commonStrat.isActive()));
                    logger.info("Found new strategy: "+ commonStrat.getName());
                }
            }
            if(commonStrat.getStrategyid()==2){
                if(!priceBreakout.containsKey(commonStrat.getName())) {
                    // priceBreakout.put(commonStrat.getName(),new PriceBreakout(commonStrat.getName(), commonStrat.getStrategyid(), commonStrat.getShortAvgTime(), commonStrat.getLongAvgTime(), commonStrat.getStdDeviationTime(), commonStrat.getNumberofOccur(), commonStrat.getTime(), commonStrat.getDate(), commonStrat.getStock(), commonStrat.getVolume(), commonStrat.getAmount(), commonStrat.getProfit(), commonStrat.getLoss(), commonStrat.isActive()));
                }
            }

        }
        setScheduler();
        appendStockMap();

    }
    public void appendStockMap(){ //If our stock is unique we add it to our stock map

        Set<String> keysBollinger = bollingers.keySet();
        Set<String> keysMovingAverage = movingAvg.keySet();
        Set<String> keysPriceBreakout = priceBreakout.keySet();
        keysBollinger.forEach((p)->stockMap.put(bollingers.get(p).getStock(),1));
        keysMovingAverage.forEach((p)->stockMap.put(movingAvg.get(p).getStock(),1));
        keysPriceBreakout.forEach((p)->stockMap.put(priceBreakout.get(p).getStock(),1));
    }

    public void setScheduler(){//Schedule the update methods for each strategy
        Set<String> keysBollinger = bollingers.keySet();
        Set<String> keysMovingAverage = movingAvg.keySet();
        Set<String> keysPriceBreakout = priceBreakout.keySet();
        keysBollinger.forEach((p)->{
            if(!(addedId.contains(p))) {
                addedId.add(p);
                scheduler.getPool().scheduleAtFixedRate(bollingers.get(p), 1000, (long) bollingers.get(p).getStdDeviationTime(), TimeUnit.MILLISECONDS);
                logger.debug("Scheduling: "+bollingers.get(p).getName() );
            }
        });

        keysMovingAverage.forEach((p)->{
            if(!(addedId.contains(p))) {
                addedId.add(p);
                scheduler.getPool().scheduleAtFixedRate(movingAvg.get(p), 1000, (long) movingAvg.get(p).getShortAvgTime(), TimeUnit.MILLISECONDS);
                logger.debug("Scheduling: "+movingAvg.get(p).getName() );
            }
        });

        keysPriceBreakout.forEach((p)->{
            if(!(addedId.contains(p))) {
                addedId.add(p);
                scheduler.getPool().scheduleAtFixedRate(priceBreakout.get(p), 1000, (long) 5000, TimeUnit.MILLISECONDS);
            }
        });
    }

    public Scheduler getScheduler() {
        return scheduler;
    }
}
