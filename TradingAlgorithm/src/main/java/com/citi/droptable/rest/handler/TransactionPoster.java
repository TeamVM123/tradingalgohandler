package com.citi.droptable.rest.handler;

import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.rest.classes.StockInfo;
import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;
import com.citi.droptable.rest.classes.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;

/**
 * This class creates a Transaction object that will be formatted into json.
 * Used to post into the data base.
 * Vincent Taylor
 */

public class TransactionPoster {

    private String path = "http://172.31.14.93";
    public TransactionPoster(String path){
        this.path = path;
    }
    public String createJson(int strategyID, String name, String symbol, int id,int buy,int volume,double price) throws JsonProcessingException {
        boolean order = buy==1?true:false;

        Transaction transaction = new Transaction(id,strategyID,name,order,price,volume,symbol,new Date(System.currentTimeMillis()),new Time(System.currentTimeMillis())); //Maps from strategy class to transaction class
        ObjectMapper map = new ObjectMapper();

       String json= map.writeValueAsString(transaction);
        return json;
    }


    public void postTransaction (String message){ //Posts to database

        try {
            URL url = new URL(path+":8080/api/transaction");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");


            DataOutputStream ds = new DataOutputStream(conn.getOutputStream());
            ds.writeBytes(message);
            ds.flush();
            ds.close();
            if (conn.getResponseCode() != 201 && conn.getResponseCode()!= 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            conn.disconnect();



        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}
