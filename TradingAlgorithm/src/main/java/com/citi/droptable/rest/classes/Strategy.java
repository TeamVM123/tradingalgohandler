package com.citi.droptable.rest.classes;

/**
 * The strategy mapped class for reading what strategies we have in our database
 * Vincent Taylor
 */

public class Strategy {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Strategy() {

    }

    public Strategy(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
