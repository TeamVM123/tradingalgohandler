package com.citi.droptable.rest.jms;

import com.citi.droptable.TradingAlgo.Bollinger;
import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.TradingAlgo.PriceBreakout;
import com.citi.droptable.rest.classes.BrokerEntity;
import com.citi.droptable.rest.classes.StockInfo;
import com.citi.droptable.rest.handler.StrategyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * First point of our JMS.
 * Producer executes all strategies and creates Broker Entity objects based on the responses.
 * Puts broker entity objects (requests) onto a queue to be read by the broker.
 */
@EnableJms
@ComponentScan
//@Service
public class Producer {
    final static Logger logger = Logger.getLogger(Producer.class);
    @Autowired
    JmsTemplate jmsTemplate;

    private static String path = "http://nyc31.conygre.com:31/";
    private static StrategyHandler handler;



    @Bean
        // Strictly speaking this bean is not necessary as boot creates a default
    JmsListenerContainerFactory<?> myJmsContainerFactory(ConnectionFactory connectionFactory) {
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        return factory;
    }

    @Bean // Serialize message content to json using TextMessage
    public MappingJackson2MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }



    public Producer(StrategyHandler handler) {
        this.handler = handler;
    }

    public  List<StockInfo> getPrice(Map<String, Integer> stocks) {
        List<StockInfo> stockInfos = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : stocks.entrySet()) {
            String stockName = entry.getKey();

            try {

                URL url = new URL(path + "/Stock/getStockPrice/" + stockName);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                conn.disconnect();

                JSONObject jsonArr = new JSONObject(response.toString());

                StockInfo stock = new ObjectMapper().readValue(jsonArr.toString(), StockInfo.class);
                stockInfos.add(stock);

            } catch (MalformedURLException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return stockInfos;

    }

    /**
     * Converts a list of current stock prices into broker entities so that they can be checked
     * if they are able to be executed or not
     * @param stockInfos
     * @return
     */
    private List<BrokerEntity> convertForBroker(List<StockInfo> stockInfos) {
        List<BrokerEntity> brokerEntities = new ArrayList<>();

        for (int i = 0; i < stockInfos.size(); i++) {
            StockInfo stock = stockInfos.get(i);
            double x = Math.random();
            int y = (int)x;
            BrokerEntity temp = new BrokerEntity(0, y, stock.getPrice(), 1000, stock.getSymbol(), stock.getTheTime(), "", stock.getSymbol(),-1);
            brokerEntities.add(temp);
        }
        return brokerEntities;

    }

    public  List<BrokerEntity> execute(List<BrokerEntity> brokerEntities) {
        Map<String, Bollinger> bollingers = handler.getBollingers();
        Map<String, MovingAvg> movingAvgs = handler.getMovingAvg();
        Map<String, PriceBreakout> priceBreakouts = handler.getPriceBreakout();

        for (Map.Entry<String, Bollinger> bollingerEntry : bollingers.entrySet()) {
            Bollinger bollinger = bollingerEntry.getValue();
            logger.debug("Bollinger about to be executed is:");
            logger.debug("Name "+ bollinger.getName() + "Looking for stock "+ bollinger.getStock() + "Active:"+ bollinger.isActive());
            for (int i=0; i< brokerEntities.size(); i++) {
                if (brokerEntities.get(i).getStock().equalsIgnoreCase(bollinger.getStock())) {
                    int executeVal = bollinger.execute(brokerEntities.get(i).getPrice(), brokerEntities.get(i).getStock(),brokerEntities.get(i).getSize());
                    if(executeVal != 0) {
                        brokerEntities.get(i).setBuy(executeVal);
                        brokerEntities.get(i).setStrategyThatExecuted(bollinger.getName());
                        brokerEntities.get(i).setStrategyId(0);
                        logger.debug("BROKER ENTITIES NOW HAS VALUE " + brokerEntities.get(i).isBuy());
                    }
                }
            }
        }

        for (Map.Entry<String, MovingAvg> movingAvgEntry: movingAvgs.entrySet()) {
            MovingAvg movingAvg = movingAvgEntry.getValue();
            for (int i=0; i< brokerEntities.size(); i++) {
                if (brokerEntities.get(i).getStock().equalsIgnoreCase(movingAvg.getStock())) {
                    int executeVal = movingAvg.execute(brokerEntities.get(i).getPrice(), brokerEntities.get(i).getStock(),brokerEntities.get(i).getSize());
                    if(executeVal != 0) {
                        brokerEntities.get(i).setBuy(executeVal);
                        brokerEntities.get(i).setStrategyThatExecuted(movingAvg.getName());
                        brokerEntities.get(i).setStrategyId(1);
                    }
                }
            }
        }

     /*   for (Map.Entry<String, PriceBreakout> priceBreakoutEntry: priceBreakouts.entrySet()) {
            PriceBreakout priceBreakout = priceBreakoutEntry.getValue();
            for (BrokerEntity brokerEntity : brokerEntities) {
                if (brokerEntity.getStock().equalsIgnoreCase(priceBreakout.getStock())) {
                    int executeVal = priceBreakout.execute(brokerEntity.getPrice(), brokerEntity.getStock(),brokerEntity.getSize());
                    brokerEntity.setBuy(executeVal);
                }
            }
        }*/

        return brokerEntities;

    }

    public  List<BrokerEntity> getForQueueForBroker(Map<String, Integer> stocks){
       //get a list of current prices from API call
        List<StockInfo> stockInfos = getPrice(stocks);
        //Take the stock info and convert it for the broker to read
        List<BrokerEntity> brokerEntities = convertForBroker(stockInfos);
        //Take the BrokerEntities requests and execute them to fill buy field
        List<BrokerEntity> readyForQueue = execute(brokerEntities);

        return  readyForQueue;
    }


    public void sendMessage(JmsTemplate jmsTemplate, List<BrokerEntity>brokerEntities){
       // BrokerEntity testBrokerEntity = new BrokerEntity(1, 1,500.0, 6,"msft",new Time(5),"");
        logger.debug("In send message Producer");

        for(BrokerEntity brokerEntity: brokerEntities) {
            MessageCreator messageCreator = new MessageCreator() {
                @Override
                public Message createMessage(Session session) throws JMSException {
                    return session.createObjectMessage(brokerEntity);
                }
            };
            logger.debug("Sending FROM PRODUCER " + messageCreator);
            jmsTemplate.send("queue/testQueue", messageCreator);
        }

        logger.debug("Leaving send message");

    }

    }



/*
    public static void main(String[] args) {
        Map<String, Integer> stocks = new HashMap<String, Integer>();
        stocks.put("msft", 1);
        stocks.put("goog", 2);

        List<StockInfo> stockInfos = new ArrayList<>();
        stockInfos = getPrice(stocks);

        List<BrokerEntity> result = convertForBroker(stockInfos);

        List<BrokerEntity> sendToQueue = execute(result);

        for (int i = 0; i < sendToQueue.size(); i++) {
            System.out.println("ID: " + result.get(i).getStock());
            System.out.println("Price " + result.get(i).getPrice());
            System.out.println("Time " + result.get(i).getTime());
            System.out.println("Buy " + result.get(i).isBuy());
        }*/


