package com.citi.droptable.rest.jms;

import com.citi.droptable.rest.classes.BrokerEntity;
import org.apache.activemq.broker.Broker;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SessionAwareMessageListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.File;
import java.sql.Time;
import java.util.List;

/**
 * This class is a broker in the JMS.
 * Acts as a middle man. Reads Broker Entities off the testQueue queue.
 * Based off the transaction request (broker entitity), it either approves or denies the request.
 * Current implementation of the broker accepts all requests off the queue and sends them to the
 * reciever, which is on a different queue.
 */
@Component
public class BrokerJms {
    final static Logger logger = Logger.getLogger(BrokerJms.class);
    /**
     * Get a copy of the application context
     */
    @Autowired
    ConfigurableApplicationContext context;

    /**
     * When you receive a message, print it out, then shut down the application.
     * Finally, clean up any ActiveMQ server stuff.
     */
    @JmsListener(destination = "queue/testQueue", containerFactory = "myJmsContainerFactory")
    public void receiveMessage(BrokerEntity brokerEntity) {

        logger.debug("Received IN BROKER <" + brokerEntity.getPrice() + ">" + " --- " + brokerEntity.getStrategyThatExecuted() + " --- " + brokerEntity.getStock()+ "--- :"+ brokerEntity.getBuy());
        MessageCreator messageCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(brokerEntity);
            }

        };
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        jmsTemplate.send("queue/recieverQueue", messageCreator);

    }
}
