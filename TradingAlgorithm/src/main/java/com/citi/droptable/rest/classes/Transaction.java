package com.citi.droptable.rest.classes;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Date;

/**
 * Transaction object that is stored in our data base.
 * This object is only to be created after the broker has approved it.
 *
 * Vincent Taylor
 */

public class Transaction implements Serializable {

    private int id;

    private int strategy;

    private String strategyName;

    private boolean buy;

    private double price;

    private int quantity;

    private String stock;

    private Date date;

    private Time time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStrategy() {
        return strategy;
    }

    public void setStrategy(int strategy) {
        this.strategy = strategy;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public boolean isBuy() {
        return buy;
    }

    public void setBuy(boolean buy) {
        this.buy = buy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Transaction(){}

    public Transaction(int id,int strategy, String strategyName, boolean buy, double price, int quantity, String stock, Date date, Time time) {
        this.id = id;
        this.strategy = strategy;
        this.strategyName = strategyName;
        this.buy = buy;
        this.price = price;
        this.quantity = quantity;
        this.stock = stock;
        this.date = date;
        this.time = time;
    }
}
