package com.citi.droptable.rest.classes;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * This object is to be used to be put on and off the queue by the broker
 * From this object we can create a transaction
 */
public class BrokerEntity implements Serializable {

    private int buy;
    private int brokerId;
    private double price;
    private int size;
    private String stock;
    private Time time;
    private String strategyThatExecuted;
    private int strategyId;
    private String symbol;
    private String stratName;

    public BrokerEntity(){}

    public BrokerEntity(int buy, int brokerId, double price, int size, String stock, Time time, String strategyThatExecuted, String symbol, int strategyId) {
        this.buy = buy;
        this.brokerId = brokerId;
        this.price = price;
        this.size = size;
        this.stock = stock;
        this.time = time;
        this.strategyThatExecuted = strategyThatExecuted;
        this.symbol=symbol;
        this.strategyId=strategyId;

    }

    public int getBuy() {
        return buy;
    }

    public int getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(int brokerId) {
        this.brokerId = brokerId;
    }

    public int getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(int strategyId) {
        this.strategyId = strategyId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStrategyThatExecuted() {
        return strategyThatExecuted;
    }

    public void setStrategyThatExecuted(String strategyThatExecuted) {
        this.strategyThatExecuted = strategyThatExecuted;
    }


    public int isBuy() {
        return buy;
    }

    public void setBuy(int buy) {
        this.buy = buy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
