package com.citi.droptable.rest.handler;

import com.citi.droptable.rest.classes.StockInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
/**
 * This class is the class for consuming price information for updating trading strategy information
 * Vincent Taylor
 *
 */
public class Consumer {
    private String path  = "http://nyc31.conygre.com:31/" ;

    public List<Double> getPrices(String stockId, int numOfSec) {//Gets the prices for a range of seconds for a single stock
        List<Double> prices = new ArrayList<>();
        try {

            URL url = new URL(path+"/Stock/getStockPriceList/" + stockId + "?" + "howManyValues=" + numOfSec);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            conn.disconnect();

            JSONArray jsonArr = new JSONArray(response.toString());

            for (int i = 0; i < jsonArr.length(); i++) {
                prices.add(new ObjectMapper().readValue(jsonArr.getJSONObject(i).toString(), StockInfo.class).getPrice());
            }


        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return prices;
    }

}