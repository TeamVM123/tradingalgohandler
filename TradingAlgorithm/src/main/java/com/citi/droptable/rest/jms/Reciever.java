package com.citi.droptable.rest.jms;

import com.citi.droptable.rest.classes.BrokerEntity;
import com.citi.droptable.rest.handler.StrategyHandler;
import com.citi.droptable.rest.handler.TransactionPoster;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;
import org.springframework.util.FileSystemUtils;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.File;

/**
 * Reciever in the JMS.
 * Reads responses off the queue from the Broker.
 * Takes the broker entity objects off the queue and turns them into transations.
 */
@Component
public class Reciever implements ErrorHandler {
    final static Logger logger = Logger.getLogger(Reciever.class);
    /**
     * Get a copy of the application context
     */
    @Autowired
    ConfigurableApplicationContext context;
    @Value("${db.url}")
    String path;
    /**
     * When you receive a message, print it out, then shut down the application.
     * Finally, clean up any ActiveMQ server stuff.
     */
    @JmsListener(destination = "queue/recieverQueue", containerFactory = "myJmsContainerFactory")
    public void receiveMessage(BrokerEntity brokerEntity) throws JsonProcessingException {

        logger.debug("Received IN RECIEVER <" + brokerEntity.getPrice() + ">" + " --- " + brokerEntity.getStrategyThatExecuted()+ " --- : " + brokerEntity.getStock());

       logger.debug("$$$$$$$$I approve the "+ brokerEntity.isBuy() + " of stock "+ brokerEntity.getStock() + "done by "+ brokerEntity.getStrategyThatExecuted());

        if(brokerEntity.isBuy() !=0) {
            TransactionPoster transactionPoster = new TransactionPoster(path);

            String jsonStr = transactionPoster.createJson(brokerEntity.getStrategyId(), brokerEntity.getStrategyThatExecuted(),
                    brokerEntity.getSymbol(), brokerEntity.getBrokerId(), brokerEntity.isBuy(),
                    brokerEntity.getSize(), brokerEntity.getPrice());
            brokerEntity.getStrategyThatExecuted();


            transactionPoster.postTransaction(jsonStr);
        }
    }

    @Override
    public void handleError(Throwable throwable) {

    }
}
