import com.citi.droptable.TradingAlgo.Bollinger;
import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.TradingAlgo.PriceBreakout;
import com.citi.droptable.rest.classes.BrokerEntity;
import com.citi.droptable.rest.classes.StockInfo;
import com.citi.droptable.rest.handler.StrategyHandler;
import com.citi.droptable.rest.jms.Producer;
import org.apache.activemq.broker.Broker;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jms.core.JmsTemplate;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestProducer {

    String stockName;
    Producer p;
    Map<String, Integer> stocksToGet = new HashMap<String, Integer>();
    StrategyHandler st =  new StrategyHandler();

    @Before
    public void setup(){
        stockName= "msft";
        p = new Producer(st);
        stocksToGet.put("Msft",1);
        stocksToGet.putIfAbsent("goog",2);
    }

    @Test
    public void testRequestForStockFromFeed(){
        List<StockInfo> stockInfos = new ArrayList<>();
        stockInfos = p.getPrice(stocksToGet);
        for(StockInfo stock : stockInfos){
            System.out.println("Company Name "+ stock.getCompanyName());
            System.out.println("Current Price "+ stock.getPrice());
            System.out.println("Current time "+ stock.getTheTime());
            System.out.println("~~~~~");
        }
    }

    @Test
    public void testRequestForStockFromFeedNullMap(){
        List<StockInfo> stockInfos = new ArrayList<>();
        stocksToGet.clear();
        stockInfos = p.getPrice(stocksToGet);
        for(StockInfo stock : stockInfos){
            System.out.println("Company Name "+ stock.getCompanyName());
            System.out.println("Current Price "+ stock.getPrice());
            System.out.println("Current time "+ stock.getTheTime());
            System.out.println("~~~~~");
        }
    }

    @Test
    public void testExecuteAll(){

/*
        Map<String,Bollinger> bollingerMap = new HashMap<>();
        Bollinger b1 = new Bollinger("test1b", 2, 0, 0 , 10000, 5000, null, null, "msft", 2000, 10000, .01, .01, true);
        bollingerMap.put(b1.getName(),b1);
        st.setBollingers(bollingerMap);

        Map<String,MovingAvg> movingAvgMap = new HashMap<>();
        MovingAvg movingAvg1 = new MovingAvg("test1m",1,1000,9000,8000,0,null,null,"goog",20000,1000000,.01,.01,true);
        movingAvgMap.put(movingAvg1.getName(), movingAvg1);
        st.setMovingAvg(movingAvgMap);


        //List of current stocks from the strategy
        List<BrokerEntity> brokerEntityList = new ArrayList<>();
        BrokerEntity testBrokerEntity1 = new BrokerEntity(1, 1,600033.0, 6,"msft",new Time(5),"");
        BrokerEntity testBrokerEntity2 = new BrokerEntity(-1, 3,6875734.0, 100,"goog",new Time(9),"");
        brokerEntityList.add(testBrokerEntity1);
        brokerEntityList.add(testBrokerEntity2);

        HashMap<String,Integer> stockMap = new HashMap<>();
        stockMap.put("msft",1);
        stockMap.put("goog",1);
        st.setStockMap(stockMap);

       List<BrokerEntity> result =  p.execute(brokerEntityList);
//need to write send message here
       for(BrokerEntity b : result){
           System.out.println("Stock is "+b.getStock());
           System.out.println("Buy is "+b.isBuy());
           System.out.println("Strategy who executed "+ b.getStrategyThatExecuted());
       }
*/
    }

}
