import com.citi.droptable.TradingAlgo.Bollinger;
import com.citi.droptable.rest.classes.Strategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertSame;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestBollinger {
    private Bollinger bollinger;
    private List<Double> prices = new ArrayList<>();

    private BigDecimal movingAverage;
    private BigDecimal upperSd;
    private BigDecimal lowerSd;

    @Before
    public void setup() {
        bollinger = new Bollinger("test", 2, 1, 5, 5000, 5000, null, null, "C", 2000, 10000, .01, .01, true);
      /*  prices.add(300.01);
        prices.add(600.08);
        prices.add(700.35);
        prices.add(360.27);
        prices.add(320.40);
        prices.add(345.06);
        prices.add(200.90);
        prices.add(187.76);*/
        prices.add(2.0);
        prices.add(7.5);
        prices.add(1.0);
        prices.add(6.0);
        prices.add(1.5);
    }

    @Test
    public void testGetSeconds(){
        assertTrue(bollinger.getSeconds(bollinger.getStdDeviationTime())==5);
    }

    @Test
    public void testStandardDeviationCalculation(){
       // BigDecimal result = new BigDecimal(2.94533530);
        double result = 2.94533530;
        BigDecimal sma = new BigDecimal(3.6);
       // assertEquals(bollinger.getStdDeviationCalculation(prices, sma), result, .2);
    }

    @Test
    public void testSmaAverage(){
       double expected = 3.6;
       //assertEquals(bollinger.getSma(prices), expected, .2);
    }

    @Test
    public void testUpdate(){
        bollinger.update(prices);
        assertEquals(bollinger.getUpperBand().doubleValue(), 9.4906, .2);
        assertEquals(bollinger.getLowerBand().doubleValue(), -2.2906, .2);
    }

    @Test
    public void testExecuteSell(){
        BigDecimal stockPrice = new BigDecimal(92.81);
        BigDecimal upper = new BigDecimal(92.61);
        BigDecimal lower = new BigDecimal(85.87);
        bollinger.setUpperBand(upper);
        bollinger.setLowerBand(lower);

        assertEquals(bollinger.execute(stockPrice.doubleValue(), "c",0), -1);
    }
//todo retest these
    @Test
    public void testExecuteBuy(){
        BigDecimal stockPrice = new BigDecimal(80.81);
        BigDecimal upper = new BigDecimal(92.61);
        BigDecimal lower = new BigDecimal(85.87);
        bollinger.setUpperBand(upper);
        bollinger.setLowerBand(lower);

        assertEquals(bollinger.execute(stockPrice.doubleValue(), "c",0), -1);
    }

    @Test
    public void testExecuteDoNothing(){
        BigDecimal stockPrice = new BigDecimal(89.81);
        BigDecimal upper = new BigDecimal(92.61);
        BigDecimal lower = new BigDecimal(85.87);
        bollinger.setUpperBand(upper);
        bollinger.setLowerBand(lower);

        assertEquals(bollinger.execute(stockPrice.doubleValue(), "c",0), -1);
    }



}
