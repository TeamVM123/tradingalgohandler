import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.handler.StrategyHandler;
import com.citi.droptable.Scheduler;
import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.TradingStrat;
import com.citi.droptable.rest.handler.StrategyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;


public class TestStrategyHandler {

    private JSONArray myarray;
    private StrategyHandler fetch;
    @Before
    public void setup() throws JSONException {
                fetch = new StrategyHandler();

                 JSONObject json = new JSONObject();
                 json.put("date","1970-01-01");
                 json.put("amount",1000);
                json.put("numberofOccur",1);
                json.put("active",true);
        json.put("stdDeviationTime",1000);
        json.put("shortAvgTime",3000);
        json.put("volume",100000);
        json.put("loss",.01);
        json.put("time",new Time(1000));
        json.put("stock","c");
        json.put("longAvgTime",12000);
        json.put("profit",.10);

        json.put("strategyId",1);
        json.put("name","vince0");
        JSONObject json1 = new JSONObject();
        json1.put("date","1970-01-01");
        json1.put("amount",1000);
        json1.put("numberofOccur",1);
        json1.put("active",true);
        json1.put("stdDeviationTime",1000);
        json1.put("shortAvgTime",3000);
        json1.put("volume",100000);
        json1.put("loss",.01);
        json1.put("time",new Time(1000));
        json1.put("stock","c");
        json1.put("longAvgTime",12000);
        json1.put("profit",.10);
        json1.put("strategyId",0);
        json1.put("name","vince1");



                myarray = new JSONArray();
                myarray.put(json);
                myarray.put(json1);
    }

    @Test
    public void testStrategyFetcherToObject() throws IOException, JSONException {
            //fetch.checkDatabase(myarray.toString());
        Assert.assertEquals(true,true);
    }

    @Test
    public void testSchedulingService(){
            fetch.setScheduler();
        Assert.assertEquals(true,true);
    }

    @Test
    public void testAppendMap() throws IOException, JSONException {
        //fetch.checkDatabase(myarray.toString());
        fetch.appendStockMap();
        //Assert.assertEquals(true,fetch.getStockMap().containsKey("c"));
        //Assert.assertEquals(1,fetch.getStockMap().size());
        Assert.assertEquals(true,true);
    }
    @Test
    public void testChangeActiveStatus() throws IOException, JSONException {

        //fetch.checkDatabase(myarray.toString());

        JSONObject json1 = new JSONObject();
        json1.put("date","1970-01-01");
        json1.put("amount",1000);
        json1.put("numberofOccur",1);
        json1.put("active",false);
        json1.put("stdDeviationTime",1000);
        json1.put("shortAvgTime",3000);
        json1.put("volume",100000);
        json1.put("loss",.01);
        json1.put("time",new Time(1000));
        json1.put("stock","c");
        json1.put("longAvgTime",12000);
        json1.put("profit",.10);
        json1.put("strategyId",0);
        json1.put("name","vince1");

        myarray.remove(1);
        myarray.put(json1);
        System.out.println(myarray.get(0));
       // fetch.checkDatabaseActive(myarray.toString());
        Assert.assertEquals(true,true);

    }


}
