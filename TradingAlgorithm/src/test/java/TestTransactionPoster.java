import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.rest.classes.Strategy;
import com.citi.droptable.rest.classes.Transaction;
import com.citi.droptable.rest.handler.TransactionPoster;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;

public class TestTransactionPoster {


    private TransactionPoster transactionPoster;
    private MovingAvg mvavg;
    private double price = 200.01;
    private int buy;
    private int volume;

    @Value("${db.url}")
    private String path;

    @Before
    public void setup(){
        mvavg = new MovingAvg("test",1,1,6000,8000,0,null,null,"c",20000,1000000,.01,.01,true);
        volume = 2000;
        transactionPoster = new TransactionPoster(path);
    }
    @Test
    public void testTransactionPosterJsonConverion() throws JSONException, IOException {
      String test ="";
       buy =1;
        try {
           test = transactionPoster.createJson(1,"test","c",1,1,100,280.8);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(test);

        Transaction transaction = new Transaction(2,1,mvavg.getName(),true,price,volume,mvavg.getStock(),new Date(System.currentTimeMillis()),new Time(System.currentTimeMillis()));
        Transaction m = new ObjectMapper().readValue(test, Transaction.class);

        Assert.assertEquals(m.getStrategyName(),transaction.getStrategyName());



    }
}
