import com.citi.droptable.TradingAlgo.MovingAvg;
import com.citi.droptable.rest.classes.Strategy;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestMovingAvg {
    private MovingAvg mvavg;
    private List<Double> prices = new ArrayList<>();
    private List<Double> prices1 = new ArrayList<>();
    private List<Double> prices2 = new ArrayList<>();
    private List<Double> prices3 = new ArrayList<>();
    @Before
    public void setup(){
        mvavg = new MovingAvg("test",1,6000,8000,8000,0,null,null,"c",20000,1000000,.01,.01,true);
        //(String name, int id, Strategy type, double shortAvgTime, double longAvgTime, double stdDeviationTime, Time time, Date date, int volume, double amount, double profit, double loss, boolean active)
        //Irregular Acting
        prices.add(300.01);
        prices.add(600.08);
        prices.add(700.35);
        prices.add(360.27);
        prices.add(320.40);
        prices.add(345.06);
        prices.add(200.90);
        prices.add(187.76);
    // Meant to act regular
        prices1.add(300.40);
        prices1.add(300.39);
        prices1.add(300.37);
        prices1.add(300.45);
        prices1.add(300.46);
        prices1.add(300.41);
        prices1.add(300.61);
    //Meant to be Buy
        prices2.add(300.01);
        prices2.add(600.08);
        prices2.add(700.35);
        prices2.add(360.27);
        prices2.add(320.40);
        prices2.add(345.06);
        prices2.add(200.90);
        prices2.add(187.76);
    // Meant to be Sell
        prices3.add(300.01);
        prices3.add(600.08);
        prices3.add(700.35);
        prices3.add(360.27);
        prices3.add(320.40);
        prices3.add(345.06);
        prices3.add(200.90);
        prices3.add(187.76);


    }
    @Test
    public void testGetSeconds(){
        assertTrue(mvavg.getSeconds(mvavg.getLongAvgTime())==8.0);
        assertTrue(mvavg.getSeconds(mvavg.getShortAvgTime())==6);
    }
    @Test
    public void testCalculateAverage(){
        mvavg.update(prices);
        TestCase.assertEquals(BigDecimal.valueOf(437.695), mvavg.getShortAvgNew());
        mvavg.update(prices);
        TestCase.assertEquals(BigDecimal.valueOf(376.85375), mvavg.getLongAvg());
    }
    @Test
    public void testTradeExecutionSelling(){

        mvavg.setShortAvgOld(  new BigDecimal(403.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(600));

        TestCase.assertEquals(mvavg.execute(400,"c",0),-1);

        mvavg.setShortAvgOld(  new BigDecimal(803.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(800));

        TestCase.assertEquals(mvavg.execute(400,"c",0),0);

        mvavg.setShortAvgOld(  new BigDecimal(403.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(800));

        TestCase.assertEquals(mvavg.execute(400,"c",0),0);



        mvavg.setShortAvgOld(  new BigDecimal(803.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(800));

        TestCase.assertEquals(mvavg.execute(400,"c",0),0);

        mvavg.setShortAvgOld(  new BigDecimal(803.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(800));

        TestCase.assertEquals(mvavg.execute(400,"c",0),0);

        mvavg.setShortAvgOld(  new BigDecimal(403.8671));
        mvavg.setShortAvgNew( new BigDecimal(700));
        mvavg.setLongAvg(new BigDecimal(600));

        TestCase.assertEquals(mvavg.execute(400,"c",0),-1);




    }


}
