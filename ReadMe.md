## Run and Setup

Trading Algorithm

1. Run the command MVN Package to build the jar file.
2. Check the Dockerfile so that the COPY command is directed to the jars output location.
3. Make sure the echo line reads the link to your database
4. Run "docker build -f Dockerfile.trade -t trade ."
5. Run "docker run --name trade trade"
6. The console should display the spring logo, and start somewhere in the range of 6 to 10 seconds depending on your machine

---

## Troubleshooting Common Errors

1. Connect Refused - The database link is most likey not on port 8080 and that the link in the dockerfile is correct
2. SQL Exception - This is an error with the https get or post request. Usually this occurs over misconfigured sql input from a manual source i.e. Postman
3. JSON Format Error - This is an error caught when the json.put method fails in transaction poster. This indicates that there was an internal library error creating the JSON. The transaction will fail but the error is non fatal. 
4. Scheduling Error - This is a fatal error that is rare, this is when the JVM fails to initalize the thread pool
5. Connection Timed Out - This is usually an error when a request has taken too long to hear back from the server. Usually indicates the caller is offline.
